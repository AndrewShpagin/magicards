#Генератор карточек для игр в стиле Magic The Gathering
Данная программа предназначена для генерации карточных игр в стиле MagicTheGathering
Для работы необходимо создать файл cards.txt, который содержит последовательность команд #class и тэгов

1) Каждой карточке в этом файле должна соответствовать одна команда #class
```
#class name_of_class
```

Эта команда объявляет класс карты. Например, Creatures, Magic и т д
Каждому классу соответствует файл описания класса, название файла соответствует имени класса, например Creatures.txt, Magic.txt.
В этом файле содержатся команды, которые генерируют изображение карты. Чуть позже опишем синтаксис.

2) cards.txt содержит набор тэгов, которые используются в файле описания класса. Тэг начинается с символа #. Имена тэгов определяет пользователь. Например


```
#class Creatures
#name Айболит
#description Добавляет игроку 1 hp при появлении на столе
#attack 0
#hitpoints 3

#class Creatures
#name Гоблин
#description Слабый, но противный
#attack 5
#hitpoints 1
```

3) В файле описания класса (в данном случае creatures.txt) содержатся команды формирования карточки. Команды могут ссылаться на тэги. Список команд:


```
#font имя_шрифта.ttf размер_шрифта цвет_текста
```

пример:
```
#font Lora-Regular.ttf 30 #00ff00
```
Шрифт должен быть в папке со скриптом. цвет - в формате #RRGGBB
```
#text x y text_width сам_по_себе_текст
```
примеры:
```
#text 36 72 50 #description
#text 36 72 100 Существо: #name
```

x,y - координаты верхнего левого угла текста, 
text_width - ширина текста в окне, которые помещаются в окошко, текст разбивается на строки,
сам_по_себе_текст может содержать просто текст, ссылку на тэги или и то, и другое
```
#сtext x y text_width сам_по_себе_текст
```	
То же самое, что и #text, но с центрированием по горизонтали
```
#image x y x1 y1 путь_к_изображению
```
Пример:
```
#image 10 10 100 100 #avatar
#image 10 10 100 100 bar.png
```
x, y - координаты верхнего левого угла изображения, которое мы размещаем на корточке.
x1, y1 - координаты правого нижнего угла изображения. Изображение масштабируется, чтобы вписаться в эти размеры.
путь_к_изображению - может содержать путь к изображению, либо ссылку на пользовательский тэг из cards.txt


# Magic Card Gaming Generator
This program is designed to generate card games in the MagicTheGathering style.
To work you need to create a file cards.txt, which contains a sequence of #class commands and tags

1) Each card in this file must correspond to one #class command.
```
#class name_of_class
```

This command declares a map class. For example, Creatures, Magic, etc.
Each class corresponds to a class description file, the file name corresponds to the class name, for example, Creatures.txt, Magic.txt.
This file contains commands that generate a map image. We will describe the syntax a little later.

2) cards.txt contains a set of tags that are used in the class description file. The tag starts with a #. Names tags defined by the user. for example

```
#class Creatures
#name Aibolit
#description Adds the player 1 hp when appearing on the table
#attack 0
#hitpoints 3

#class Creatures
#name Goblin
#description Weak but nasty
#attack 5
#hitpoints 1
```

3) The class definition file (in this case, creatures.txt) contains the card formation commands. Commands can refer to tags. List of commands:

```
#font font_name.ttf font_size text_color

```

For example:
```
#font Lora-Regular.ttf 30 #00ff00
```

The font should be in the folder with the script. color - in the format #RRGGBB
```
#text x y text_width the_text
```
For example:
```
#text 36 72 50 #description
#text 36 72 100 Creature: #name
```

x, y - the coordinates of the upper left corner of the text,
text_width - maximum text width oin the line, the text is divided into lines,
the_text may contain just a text, a link to tags or bot
```
#сtext x y text_width the_text
```	
Same as #text, but horizontally centered
```
#image x y x1 y1 the_path_to_image
```
For example:
```
#image 10 10 100 100 #avatar
#image 10 10 100 100 bar.png
```

x, y - the coordinates of the upper left corner of the image, which we place on the squat.
x1, y1 - coordinates of the lower right corner of the image. The image is scaled to fit into these dimensions.
the_path_to_image - may contain the path to the image, or a link to the user tag from cards.txt