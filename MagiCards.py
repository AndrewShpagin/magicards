#look the description in the readme.md

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
from image_utils import TextWrapper
import textwrap

# text lines from the cards.txt
lines = []
# currently created card index
cardindex = 0
# the default font
font = ImageFont.truetype("default.ttf", 16, encoding='cp1251')
# the text color
color="#000000"

# read the tag in the text line
def command(str):
	return str.split(" ")[0];

# read the iteems after the tag, index = 0 - tag itself, index = 1 - forst parameter, etc
def param(str,index):
	return str.split(" ")[index]

# read multiple parameters at once starting from the index. All parameters are combined into single text line, all tags replaced with text from the cards.txt
def multi_param(str,index, tag_search_index):
	global lines
	list1 = str.split(" ")
	str = ""
	for i in range(index,len(list1)):
		substr = list1[i]
		if substr[0] == "#" :
			for i in range(tag_search_index + 1,len(lines)):
				s1=lines[i];
				if command(s1) == substr:
					substr = s1[len(substr) + 1:]
					break
		str += substr
		str += " "
	return str

# create the card image and save into the file. param_position is the line index in cards.txt
def make_card_for_class(name_of_class, param_position):
	global cardindex
	global color
	imgname = name_of_class + ".jpg"
	img = Image.open(imgname)
	draw = ImageDraw.Draw(img)
	txt = name_of_class + ".txt"
	cardindex += 1
	print("\n[making the card ##",cardindex,"]");
	# parse the commands from the class description file
	with open (txt, 'rt') as myfile:
		for myline in myfile:
			line=myline.rstrip()
			com=command(line)
			imgname1 = ""			
			if com == "#image":
				# the #image command
				x = int(param(line,1))
				y = int(param(line,2))
				x1 = int(param(line,3))
				y1 = int(param(line,4))
				# read the image path
				imgname1 = multi_param(line,5,param_position)
				# open the image to place onto the card
				img1 = Image.open(imgname1)
				w, h = img1.size;
				ww = x1 - x + 1
				hh = y1 - y + 1
				if w/ww > h/hh :
					h *= ww / w
					w = ww
				else:
					w *= hh/h
					h = hh
				# scale the image to fit into (x,y)-(x1,y1)
				img1 = img1.resize((int(w),int(h)),Image.ANTIALIAS)
				# place on the canvas img
				img.paste(img1, (int(x + (ww - w)/2.0), int(y + (hh - h)/2.0)), mask=img1)
				print("#image",x,y,x1,y1,imgname1)
			if com == "#font":
				# the #font command
				font = ImageFont.truetype(param(line,1), int(param(line,2)), encoding='cp1251')
				color=param(line,3)
				print(line)
			if com == "#text" or  com == "#ctext":
				# the #text/ctext commands
				x = int(param(line,1))
				y = int(param(line,2))
				col=int(param(line,3))
				text = multi_param(line,4,param_position)
				# wrap text onto the separate lines and draw on the card
				wrapper = TextWrapper(text, font, col)
				wrapped_text = wrapper.wrapped_text()
				align = 'center'
				if com == '#text': align = 'left'
				draw.multiline_text((x,y),wrapped_text + "\n",fill=color,font=font,align=align)			
				print(com,x,y,col,text)
	img1 = "cards/card" + str(cardindex) + ".png"
	print("[ Card created:",img1,"]")
	# save the resulting image
	img.save(img1)

#read cards.txt into lines
with open ('cards.txt', 'rt') as myfile:
	for myline in myfile:
		lines.append(myline.rstrip())

#run through cards.txt
line=0
for myline in lines:
	com=command(myline)
	if com == "#class":
		make_card_for_class(param(myline,1), line)
	line+=1